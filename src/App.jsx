import React from "react";
import "./scss/_custom.scss";
import "bootstrap/js/dist/tooltip";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import "../node_modules/nprogress/nprogress.css";

import Sidebar from "./layout/sidebar";
import Dashboard from "./layout/dashboard";
import Setup from "./layout/setup";
import Logs from "./layout/logs";
import License from "./layout/license";
import Notfound from "./layout/notfound";
import Login from "./layout/login";
import { consoleLog } from "./function/console";
import Admin from "./layout/admin";
import { checkToken } from "./function/Graphql";

class App extends React.Component {
    state = {
        isLogin: true,
    };

    checkLogin = () => {
        setInterval(async () => {
            let loggedin = localStorage.getItem("wizcard");
            loggedin = JSON.parse(loggedin);
            let state = { ...this.state };
            if (!loggedin || loggedin.token == undefined || loggedin == null) {
                state.isLogin = false;
                this.setState(state);
            } else {
                let ctok = await checkToken(loggedin.key);
                if (ctok == false || ctok == null || !ctok) {
                    localStorage.removeItem("wizcard");
                    state.isLogin = false;
                    this.setState(state);
                }
                if (parseInt(loggedin.token.expired) < Date.now()) {
                    localStorage.removeItem("wizcard");
                    state.isLogin = false;
                    this.setState(state);
                }
            }
        }, 5000);
    };

    componentDidMount() {
        this.checkLogin();
        consoleLog(
            "%cWellcome to Wizcard",
            "font-weight: bold; font-size: 50px;color: dodgerblue; text-shadow: 3px 3px 0 rgb(217,31,38) , 6px 6px 0 rgb(226,91,14) , 9px 9px 0 rgb(245,221,8) , 12px 12px 0 rgb(5,148,68) , 15px 15px 0 rgb(2,135,206) , 18px 18px 0 rgb(4,77,145) , 21px 21px 0 rgb(42,21,113)"
        );
        consoleLog("%c WARNING!!", "font-size: 25px; color: rgb(179, 179, 5);");
        consoleLog(
            "%c This tools is for developer only, please don't make any change here. otherwise your account will be deleted",
            "color: blue; font-size: 15px;"
        );
    }
    render() {
        return <>{!this.state.isLogin ? <Login /> : <Apps />}</>;
    }
}

class Apps extends React.Component {
    render() {
        return (
            <div className="container-fluid mybody">
                <Router>
                    <div className="row">
                        <Sidebar />
                        <div className="col pointdash">
                            <Switch>
                                <Route path="/setup">
                                    <Setup />
                                </Route>
                                <Route exact path="/">
                                    <Dashboard />
                                </Route>
                                <Route path="/logs">
                                    <Logs />
                                </Route>
                                <Route path="/license">
                                    <License />
                                </Route>
                                <Route path="/admin">
                                    <Admin />
                                </Route>
                                <Route path="*" exact={true}>
                                    <Notfound />
                                </Route>
                            </Switch>
                        </div>
                    </div>
                </Router>
            </div>
        );
    }
}

export default App;
