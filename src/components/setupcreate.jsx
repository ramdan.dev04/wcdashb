import React from "react";
import Setupform from "./setupform";

class Setupcreate extends React.Component {
    mRef = React.createRef(null);
    render() {
        return (
            <div className="bg-white d-flex justify-content-center p-3">
                <button
                    className="btn btn-primary"
                    data-bs-toggle="modal"
                    data-bs-target="#exampleModal"
                >
                    Launch App
                </button>
                <div
                    ref={this.mRef}
                    className="modal fade"
                    id="exampleModal"
                    tabIndex="-1"
                    aria-labelledby="exampleModalLabel"
                    aria-hidden="true"
                >
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div
                                className="modal-header"
                                style={{ border: "none" }}
                            >
                                <button
                                    type="button"
                                    className="btn-close"
                                    data-bs-dismiss="modal"
                                    aria-label="Close"
                                ></button>
                            </div>
                            <div className="modal-body">
                                <Setupform modal={this.mRef} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default Setupcreate;
