import React from "react";
import NProgress from "nprogress";

class Logs extends React.Component {
    componentWillUnmount() {
        NProgress.start();
    }
    componentDidMount() {
        NProgress.done();
    }
    render() {
        return (
            <div className="logs container">
                <h1>Logs</h1>
                <div className="mt-5 bg-white rounded w-100 p-2">
                    <div className="d-flex justify-content-center text-secondary">
                        Nothing here
                    </div>
                </div>
            </div>
        );
    }
}

export default Logs;
