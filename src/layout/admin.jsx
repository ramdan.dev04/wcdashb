import React from "react";
import NProgress from "nprogress";
import { checkAdmin } from "../function/Graphql";

class Admin extends React.Component {
    componentWillUnmount = () => {
        NProgress.start();
    };
    componentDidMount = async () => {
        let check = await checkAdmin();
        if (!check) {
            window.location.replace("../../");
        }
        NProgress.done();
    };
    render() {
        return (
            <div className="admin container">
                <h1>Admin</h1>
                <div className="mt-5 bg-white rounded w-100 p-2">
                    <div className="d-flex justify-content-center text-secondary">
                        Nothing here
                    </div>
                </div>
            </div>
        );
    }
}

export default Admin;
