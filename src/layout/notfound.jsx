function Notfound() {
    return (
        <div className="d-flex justify-content-center mt-5">
            <p className="text-white">404 Not Found</p>
        </div>
    );
}

export default Notfound;
