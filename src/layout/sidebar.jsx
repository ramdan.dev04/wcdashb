import React from "react";
import navBrand from "../assets/image/navbrand.png";
import Navlink from "../components/navlink";

class Sidebar extends React.Component {
    render() {
        return (
            <div className="col mywhite sidebar">
                <div className="navbrand d-flex justify-content-center">
                    <img src={navBrand} alt="" />
                </div>
                <Navlink setPath={this.props.setPath} />
                <div className="navfooter">
                    <p>&copy; Wizcard 2021</p>
                </div>
            </div>
        );
    }
}

export default Sidebar;
