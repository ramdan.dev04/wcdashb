import React, { Component } from "react";
import NProgress from "nprogress";
import navBrand from "../assets/image/navbrand.png";
import Swal from "sweetalert2";
import { checkLicense, checkToken, createToken } from "../function/Graphql";

class Login extends Component {
    state = {
        isDisabled: false,
    };
    keyRef = React.createRef(null);
    componentDidMount() {
        NProgress.done();
    }
    disableForm = (value) => {
        let state = { ...this.state };
        state.isDisabled = value;
        this.setState(state);
    };
    loggedIn = (res) => {
        let data = {};
        data.license = res.license;
        data.login = true;
        localStorage.setItem("wizcard", JSON.stringify(data));
    };

    postHandler = async (e) => {
        e.preventDefault();
        let value = this.keyRef.current.value;
        this.disableForm(true);
        let clic = await checkLicense(value);
        if (!clic) {
            this.disableForm(false);
            Swal.fire(
                "Login Failed",
                "We cant validate your license key!!",
                "error"
            );
        } else {
            localStorage.setItem("wizcard", JSON.stringify(clic));
            let checktok = await checkToken(clic.key);
            if (!checktok) {
                let cretok = await createToken(clic.key);
                if (!cretok) {
                    this.disableForm(false);
                    Swal.fire("Error", "Something went wrong", "error");
                } else {
                    let loc = localStorage.getItem("wizcard");
                    loc = JSON.parse(loc);
                    loc.token = cretok;
                    localStorage.setItem("wizcard", JSON.stringify(loc));
                    window.location.reload();
                }
            } else {
                let loc = localStorage.getItem("wizcard");
                loc = JSON.parse(loc);
                loc.token = checktok;
                localStorage.setItem("wizcard", JSON.stringify(loc));
                window.location.reload();
            }
        }
    };
    render() {
        return (
            <div className="container h-100">
                <div className="d-flex formContainer justify-content-center align-items-center">
                    <div className="loginForm bg-dark p-2 rounded">
                        <div className="text-center">
                            <img src={navBrand} alt="" />
                        </div>
                        <form
                            method="post"
                            onSubmit={
                                this.state.isDisabled
                                    ? (e) => e.preventDefault()
                                    : this.postHandler
                            }
                            className="mt-3"
                        >
                            <div className="mb-3 text-center">
                                <label
                                    htmlFor="licensekey"
                                    className="text-primary"
                                >
                                    License Key
                                </label>
                                <input
                                    ref={this.keyRef}
                                    type="text"
                                    className="form-control"
                                    id="licensekey"
                                    required
                                />
                            </div>
                            <div className="text-center">
                                <button
                                    type="submit"
                                    className="btn btn-primary"
                                >
                                    Sign In
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
