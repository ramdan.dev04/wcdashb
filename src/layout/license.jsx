import React from "react";
import NProgress from "nprogress";
import moment from "moment";

class License extends React.Component {
    state = {
        license: {},
    };
    componentWillUnmount() {
        NProgress.start();
    }
    componentDidMount() {
        let local = localStorage.getItem("wizcard");
        if (!local || local == null) {
            return window.location.replace("../../");
        }
        local = JSON.parse(local);
        let state = { ...this.state };
        state.license = local;
        this.setState(state);
        NProgress.done();
    }
    render() {
        return (
            <div className="license container">
                <h1>License</h1>
                <div className="mt-5 p-5 card">
                    <div className="text-center">
                        <h4>Wizcard License</h4>
                    </div>
                    <hr />
                    <div className="row">
                        <div className="col">
                            <p>Type</p>
                            <p>Models</p>
                            <p>Registered</p>
                            <p>Expires</p>
                        </div>
                        <div className="col">
                            <p>:</p>
                            <p>:</p>
                            <p>:</p>
                            <p>:</p>
                        </div>
                        <div className="col text-primary">
                            <p>{this.state.license.type}</p>
                            <p>{this.state.license.model}</p>
                            <p>
                                {moment(
                                    parseInt(this.state.license.registered)
                                ).format("MMMM Do YYYY")}
                            </p>
                            <p>
                                {moment(
                                    parseInt(this.state.license.expired)
                                ).format("MMMM Do YYYY")}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default License;
